from base64 import b64decode, b64encode
from datetime import datetime
from enum import Enum
from logging import Logger
from pathlib import Path
from typing import Any, Union


class ColumnType(Enum):
    INTEGER = int
    FLOAT = float
    TEXT = str
    BYTES = bytes
    TIMESTAMP = int
    BOOL = bool
    DATETIME = datetime


class Column:
    def __init__(self, name: str, col_type: ColumnType, nullable=True, indexed=False):
        self.name = name
        self.col_type = col_type
        self.nullable = nullable if not indexed else True
        self.indexed = indexed

    def __repr__(self) -> str:
        return (f'<Column name={self.name}, col_type={self.col_type}, nullable={self.nullable}'
                f', indexed={self.indexed}, at 0x{id(self):X}>')


class Table:
    def __init__(self, name: str, columns: list[Column], initial_entries: list[tuple] = None, with_id=True):
        self.name = name
        self.additional_columns = columns
        self.columns: list[Column] = (
            ([Column('id', ColumnType.INTEGER, nullable=False, indexed=True)] if with_id else []
             ) + columns + [
                 Column('create_time', ColumnType.DATETIME, nullable=False),
                 Column('update_time', ColumnType.DATETIME)
            ])
        self.column_dict: dict[str, int] = {col.name: col_id for col_id, col in enumerate(self.columns)}
        self.initial_entries: list[tuple] = initial_entries if initial_entries else []
        self.with_id = with_id

    def __repr__(self) -> str:
        return (f'<Table name={self.name}, columns={[col.name for col in self.columns]}, with_id={self.with_id}'
                f' at 0x{id(self):X}>')


class Database:
    def __init__(self, tables: list[Table]):
        self.tables = tables
        self.table_dict: dict[str, Table] = {table.name: table for table in self.tables}


class Sdb:
    SEPARATOR = '\t'

    def __init__(self, database: Database, database_path: Path, logger: Logger = None):
        self._database = database
        self.database_path = database_path
        self.lock_path = database_path / "db.lock"
        self.logger = logger

        self.connected = False
        self._data: dict[str, list[list[Any]]] = {}
        self._table_max_id: dict[str, int] = {}
        # self._indexes[table_name][column_name][value] = row_index
        self._indexes: dict[str, dict[str, dict[Any, list[int]]]] = {}

    def __del__(self):
        self.release()

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, _type, _value, _traceback):
        self.release()

    def connect(self):
        if not self.database_path.exists():
            self.database_path.mkdir(parents=True)

        if self.lock_path.exists():
            raise RuntimeError(f'Cannot connect to database at {self.database_path} : locked')
        self.lock_path.touch()
        self.connected = True
        for table in self._database.tables:
            table_data: list[list[Any]] = []
            max_id = 0
            table_indexes: dict[str, dict[Any, list[int]]] = {}
            indexed_columns: list[tuple[str, int]] = []
            for col_id, col in enumerate(table.columns):
                if col.indexed:
                    table_indexes[col.name] = {}
                    indexed_columns.append((col.name, col_id))
            if (self.database_path / (table.name + '.csv')).exists():
                with open(self.database_path / (table.name + '.csv'), 'r', encoding='utf-8') as table_file:
                    row = table_file.readline()
                    values = row.split(self.SEPARATOR)
                    line_count = 0
                    if len(values) != len(table.columns):
                        raise RuntimeError(f'Cannot read table "{table.name}": wrong column count (header)')
                    row = table_file.readline()
                    line_count += 1
                    while row:
                        values = row[:-1].split(self.SEPARATOR)
                        if len(values) != len(table.columns):
                            raise RuntimeError(
                                f'Cannot read table "{table.name}": wrong column count at line {line_count}')
                        new_values = []
                        for val, col in zip(values, table.columns):
                            if not val:
                                new_values.append(None)
                            else:
                                if col.col_type.value == bytes:
                                    new_values.append(b64decode(val.encode()))
                                elif col.col_type.value == datetime:
                                    new_values.append(datetime.fromisoformat(val))
                                elif col.col_type.value == str:
                                    new_values.append(val)
                                else:
                                    new_values.append(col.col_type.value(val))
                        table_data.append(new_values)
                        if table.with_id:
                            if max_id is None or new_values[0] > max_id:
                                max_id = new_values[0]
                        for col_name, col_id in indexed_columns:
                            if new_values[col_id] in table_indexes[col_name]:
                                table_indexes[col_name][new_values[col_id]].append(line_count - 1)
                            else:
                                table_indexes[col_name][new_values[col_id]] = [line_count - 1]

                        row = table_file.readline()
                        line_count += 1

            self._data[table.name] = table_data
            if table.with_id:
                self._table_max_id[table.name] = max_id
            if table_indexes:
                self._indexes[table.name] = table_indexes

    def release(self):
        if self.connected:
            if self.lock_path.exists():
                self.lock_path.unlink()
            self.connected = False

    def create_table(self, table: Table):
        table_path = self.database_path / (table.name + '.csv')
        if not table_path.parent.exists():
            table_path.parent.mkdir(parents=True)
        if not table_path.exists():
            with open(table_path, 'w', encoding='utf-8') as table_file:
                table_file.write(self.SEPARATOR.join([column.name for column in table.columns]) + '\n')
        else:
            raise RuntimeError(f'Table {table} already exists at path "{table_path}"')

    def get_table(self, table_name: str) -> Table:
        return self._database.table_dict[table_name]

    def _save_table(self, table: Table, append_rows: int):
        file_path = self.database_path / (table.name + '.csv')
        table_data = self._data[table.name]
        if append_rows > 0:
            with open(file_path, 'a', encoding='utf-8') as table_file:
                for row in table_data[-append_rows:]:
                    row_values = []
                    for value in row:
                        if value is None:
                            row_values.append('')
                        elif isinstance(value, bytes):
                            row_values.append(b64encode(value).decode())
                        elif isinstance(value, datetime):
                            row_values.append(value.isoformat())
                        else:
                            row_values.append(str(value))
                    table_file.write(self.SEPARATOR.join(row_values) + '\n')
        else:
            temp_path = self.database_path / ('.table.name' + '_swap.csv')
            with open(temp_path, 'w', encoding='utf-8') as temp_file:
                temp_file.write(self.SEPARATOR.join([column.name for column in table.columns]) + '\n')
                for row in table_data:
                    temp_file.write(self.SEPARATOR.join([
                        b64encode(value).decode() if isinstance(value, bytes) else str(value)
                        if value is not None else ''
                        for value in row]) + '\n')
            file_path.unlink()
            temp_path.rename(file_path)

    def _select_ids(self, rows: dict[str, Any], table: Table, limit: int = None, offset: int = 0) -> list[int]:
        unindexed_keys: list[tuple[int, Any]] = []
        row_ids: set[int] = None
        if rows is not None:
            if table.name in self._indexes:
                table_indexes = self._indexes[table.name]
                for col_name, value in rows.items():
                    if col_name in table_indexes:
                        if isinstance(value, list):
                            for subvalue in value:
                                if subvalue not in table_indexes[col_name]:
                                    continue
                                if row_ids is None:
                                    row_ids = set(table_indexes[col_name][subvalue])
                                else:
                                    row_ids.intersection_update(set(table_indexes[col_name][subvalue]))
                        else:
                            if value not in table_indexes[col_name]:
                                return []
                            if row_ids is None:
                                row_ids = set(table_indexes[col_name][value])
                            else:
                                row_ids.intersection_update(set(table_indexes[col_name][value]))
                    else:
                        unindexed_keys.append((table.column_dict[col_name], value))
            else:
                unindexed_keys = [(table.column_dict[col_name], value) for col_name, value in rows.items()]

        table_data = self._data[table.name]
        if row_ids is None:
            if limit is None:
                row_ids = range(offset, len(table_data))
            else:
                row_ids = range(offset, min(len(table_data), limit))
        elif limit is not None or offset > 0:
            if limit is None:
                row_ids = row_ids[offset:]
            else:
                row_ids = row_ids[offset:offset + limit]

        results: list[int] = []
        for row_id in row_ids:
            row = table_data[row_id]
            row_match = True
            for col_id, value in unindexed_keys:
                if isinstance(value, list):
                    if row[col_id] not in value:
                        row_match = False
                        break
                else:
                    if row[col_id] != value:
                        row_match = False
                        break
            if row_match:
                results.append(row_id)

        return results

    def insert(self, entry: Union[dict, list, tuple], table: Union[Table, str]) -> int:
        if isinstance(table, str):
            table = self.get_table(table)

        values: list[Any] = []
        indexed_columns: list[tuple[str, int]] = [
            (col.name, col_id) for col_id, col in enumerate(table.columns) if col.indexed]
        # If entry given as list : check for column types matching
        if isinstance(entry, (list, tuple)):
            for value, column in zip(entry, table.additional_columns):
                if (column.nullable and value is None) or isinstance(value, column.col_type.value):
                    continue
                raise RuntimeError(f'SDB : cannot insert entry "{entry}" in table {table.name}:'
                                   f' {value} is not an instance of {column.col_type.value}')
            values = entry
        # If entry given as dict : creating list
        elif isinstance(entry, dict):
            for column in table.additional_columns:
                if column.name in entry:
                    if (column.nullable and entry[column.name] is None) or isinstance(
                            entry[column.name], column.col_type.value):
                        values.append(entry[column.name])
                    else:
                        raise RuntimeError(f'SDB : cannot insert entry "{entry}" in table {table.name}:'
                                           f' {entry[column.name]} is not an instance of {column.col_type.value}')
                elif column.nullable:
                    values.append(None)
                else:
                    raise RuntimeError(
                        f'SDB : cannot insert entry "{entry}" in table {table.name}:'
                        f' {column.name} is missing (not nullable)')
        else:
            raise RuntimeError(f'SDB : entry must be tuple or list or dict : {entry} is {type(entry)}')

        # Making a new row: id(optional) + values + create_time + update_time
        new_row: list[Any] = []
        new_id = 1
        if table.with_id:
            new_id = self._table_max_id[table.name] + 1
            self._table_max_id[table.name] = new_id
            new_row.append(new_id)
        new_row += [*values, datetime.now(), None]
        # Add new row in data (memory)
        self._data[table.name].append(new_row)
        # Check for index to update
        if indexed_columns:
            for col_name, col_id in indexed_columns:
                if new_row[col_id] in self._indexes[table.name][col_name]:
                    self._indexes[table.name][col_name][new_row[col_id]].append(len(self._data[table.name]) - 1)
                else:
                    self._indexes[table.name][col_name][new_row[col_id]] = [len(self._data[table.name]) - 1]

        self._save_table(table, 1)
        if self.logger is not None:
            self.logger.debug(f'SDB : insert {entry} in {table} => {new_id}')
        return new_id

    def select(self, table: Union[str, Table],
               columns: Union[list[int], list[str]] = None,
               rows: dict[str, Any] = None,
               limit: int = None,
               offset: int = 0) -> list[list[Any]]:
        if isinstance(table, str):
            table = self.get_table(table)
        row_ids = self._select_ids(rows, table, limit=limit, offset=offset)

        table_data = self._data[table.name]
        results: list[list[Any]] = []
        if columns is not None:
            column_ids = [val if isinstance(val, int) else table.column_dict[val] for val in columns]
            for row_id in row_ids:
                results.append([table_data[row_id][col_id] for col_id in column_ids])
        else:
            for row_id in row_ids:
                results.append(table_data[row_id])
        if self.logger is not None:
            self.logger.debug(f'SDB : select ({columns}, {rows}) in {table} => {results}')
        return results

    def select_dict(self, table: Union[str, Table],
                    columns: Union[list[int], list[str]] = None,
                    rows: dict[str, Any] = None,
                    limit: int = None,
                    offset: int = None) -> list[dict[str, Any]]:
        if isinstance(table, str):
            table = self.get_table(table)
        row_ids = self._select_ids(rows, table, limit=limit, offset=offset)

        table_data = self._data[table.name]
        results: list[list[Any]] = []
        if columns is not None:
            column_ids = [val if isinstance(val, int) else table.column_dict[val] for val in columns]
            for row_id in row_ids:
                new_row = {}
                for col_id in column_ids:
                    new_row[table.columns[col_id].name] = table_data[row_id][col_id]
                results.append(new_row)
        else:
            for row_id in row_ids:
                results.append({column.name: value for column, value in zip(table.columns, table_data[row_id])})
        if self.logger is not None:
            self.logger.debug(f'SDB : select_dict ({columns}, {rows}) in {table} => {results}')
        return results

    def update(self, rows: dict[str, Any], values: dict[str, Any], table: Union[str, Table]) -> list[int]:
        if isinstance(table, str):
            table = self.get_table(table)
        row_ids = self._select_ids(rows, table)

        table_data = self._data[table.name]
        for row_id in row_ids:
            row = table_data[row_id]
            for col_name, value in values.items():
                column = table.columns[table.column_dict[col_name]]
                if value is None:
                    if not column.nullable:
                        raise RuntimeError(
                            f'SDB : cannot nullify column "{col_name}" in table {table.name} (not nullable)')
                elif not isinstance(value, column.col_type.value):
                    raise RuntimeError(f'SDB : cannot update column "{col_name}" in table {table.name}:'
                                       f' {value} is not an instance of {column.col_type.value}')
                row[table.column_dict[col_name]] = value

        self._save_table(table, 0)
        if self.logger is not None:
            self.logger.debug(f'SDB : update {rows} with {values} in {table} => {row_ids}')
        return row_ids

    def delete(self, rows: dict[str, Any], table: Union[str, Table]) -> list[int]:
        if isinstance(table, str):
            table = self.get_table(table)
        row_ids = self._select_ids(rows, table)

        table_data = self._data[table.name]
        for row_id in row_ids:
            table_data.pop(row_id)

        self._save_table(table, 0)
        if self.logger is not None:
            self.logger.debug(f'SDB : delete {rows} in {table} => {row_ids}')
        return row_ids

    def _get_table_names(self) -> list[str]:
        return [str(entry.relative_to(self.database_path).parent / entry.stem)
                for entry in self.database_path.rglob('*.csv')]

    def _table_exists(self, name: str) -> bool:
        return (self.database_path / (name + '.csv')).exists()

    def _read_columns(self, table_name: str) -> list[str]:
        if not self._table_exists(table_name):
            raise RuntimeError(f'SDB : Cannot read columns for table {table_name}: table not found')
        with open(self.database_path / (table_name + '.csv'), 'r', encoding='utf-8') as table_file:
            return table_file.readline()[:-1].split(self.SEPARATOR)

    def _check_table(self, table: Table) -> bool:
        for col_name, column in zip(self._read_columns(table.name), table.columns):
            if col_name != column.name:
                return False
        return True

    def _initialize_tables(self, create_tables: bool):
        database_table_names = self._get_table_names()
        for table in self._database.tables:
            if table.name in database_table_names:
                if not self._check_table(table):
                    raise RuntimeError(f'SDB : table "{table.name}" not valid')
                continue

            if not create_tables:
                raise RuntimeError(f'SDB : table {table.name} is missing')

            if self.logger is not None:
                self.logger.info(f'SDB : creating table "{table.name}"')
            self._data[table.name] = []
            if table.with_id:
                self._table_max_id[table.name] = 0
            for column in table.columns:
                if column.indexed:
                    if table.name not in self._indexes:
                        self._indexes[table.name] = {}
                    self._indexes[table.name][column.name] = {}
            self.create_table(table)
            for entry in table.initial_entries:
                self.insert(entry, table)

    def initialize_database(self, create_tables: bool):
        self._initialize_tables(create_tables)
